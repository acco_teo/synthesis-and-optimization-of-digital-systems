
proc slack_histogram {leftEdge rightEdge} {

	set o { 0 0 0 0 0 0 0 0 0 0 }
	set window [ expr $rightEdge - $leftEdge]
	set cells [get_cell *]
	foreach_in_collection cell $cells {
		set out_pins [get_pins -of_object $cell -filter {direction==out}]
		foreach_in_collection out_pin $out_pins {
			set slack [get_attribute $out_pin max_slack]
			if { $slack > $leftEdge  && $slack < $rightEdge } {
				for {set i 0} {$i < 10} {incr i} {
					if { $slack > [expr $i.0 * $window.0 / 10] && $slack < [expr ($i.0+1) * $window.0 / 10] } {
						lset o  $i [expr 1 + [ lindex $o  $i] ]
							break;
					}
				}
			}
		}
	}
	puts "$o"
}
