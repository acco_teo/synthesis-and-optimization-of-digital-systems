set wrt_path_collection [get_timing_paths -pba_mode exhaustive -slack_lesser_than 1000000000000]
set prev_arrival 0
set out "out"

puts ""
puts -nonewline [format "%30s" "cell_name" ]
puts -nonewline [format "%30s" "incremental_arrival" ]
puts [format "%30s" "arrival"]
puts -nonewline [format "%10s" " "]
puts "---------------------------------------------------------------------------------"


foreach_in_collection timing_point [get_attribute $wrt_path_collection points] {
	set cell_name [get_attribute [get_attribute $timing_point object] full_name]
	set dir [get_attribute [get_attribute $timing_point object] direction]
	if { $dir eq $out } {
		set arrival [get_attribute $timing_point arrival]
		set diff [expr $arrival - $prev_arrival]
		puts -nonewline [format "%30s" $cell_name ]
		puts -nonewline [format "%30f" $diff ]
		puts [format "%30f" $arrival]
		set prev_arrival $arrival
	}
}

puts "\n"
