#/export/home/stud/s213824/Documents/SODS/Lab3/WORK_SYNTHESIS
#before dc_shell-xg-t source ./scripts/synthesis.tcl
#before pt_shell source ./scripts/pt_analysis.tcl
#The command receives as input the name of the cell and returns the list of
#attributes of that cell. If the cell does not exist the returned list is empty.
#The returned list contains the following items:
#(a) full_name
#(b) reference_name
#(c) area
#(d) size (i.e., the number in the reference name after the character 'X')
#(e) leakage_power
#(f) dynamic_power
#(g) total_power
#(h) arrival_time (i.e. the maximum between max_rise_arrival and max_fall_arrival at the output pin of the cell)
#(i) max _ slack (i.e., slack of the most critical path through the output pin of the cell)

proc get_cell_attributes {cellName} {
	set attributes_names [list "full_name" "reference_name" "area" "size" "leakage_power" "dynamic_power" "total_power" "max(max_rise_arrival, max_fall_arrival)" "max_slack"]

	set attributes_list ""
	lappend attributes_list [get_attribute [get_cell $cellName] full_name]
	set rn [get_attribute [get_cell $cellName] ref_name]
	lappend attributes_list $rn
	lappend attributes_list [get_attribute [get_cell $cellName] area]

	set size ""
	regexp {[a-zA-Z0-9\_]+X([0-9]+)} $rn matchresult size
	lappend attributes_list $size
	lappend attributes_list [get_attribute [get_cell $cellName] leakage_power]
	lappend attributes_list [get_attribute [get_cell $cellName] dynamic_power]
	lappend attributes_list [get_attribute [get_cell $cellName] total_power]

	set pin_o [get_pins -filter {direction == out} -of_object [get_cell $cellName]]
	set mra [get_attribute [get_pin $pin_o] max_rise_arrival]
	set mfa [get_attribute [get_pin $pin_o] max_fall_arrival]
	if { $mra > $mfa } { lappend attributes_list $mra } else { lappend attributes_list $mfa }

	lappend attributes_list [get_attribute $pin_o max_slack]

	puts ""
	puts ""
	for {set x 0} {$x < [llength $attributes_list]} {incr x} {
		set name [lindex $attributes_names $x]
		set value [lindex $attributes_list $x]
		puts "	$name: $value"
	}

}
