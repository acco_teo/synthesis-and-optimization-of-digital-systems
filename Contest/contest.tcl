set timing_save_pin_arrival_and_slack true

suppress_message PWR-602
suppress_message PTE-139
suppress_message LNK-041
suppress_message PWR-601
suppress_message PWR-246
suppress_message NED-005
suppress_message NED-040

####################################################################
#	procedure cell_swap_to_LVT { cell_to_swap }
#	# swap the cell
#	Return: difference of leakage
#
proc cell_swap_to_LVT { cell } {## size rimane uguale e cambia threshold voltage (LVT HVT)
	set initial_leakage [get_attribute [get_cell $cell] leakage_power]
	set libcell [get_attribute [get_lib_cells -of_object $cell] base_name]
	set alternatives [get_alternative_lib_cells -libraries CORE65LPLVT $cell]
	foreach_in_collection el $alternatives {
		set name [get_attribute $el base_name]
		set pattern_to_match [string range $name end-3 end]
		set pattern_ref [string range $libcell end-3 end]
		if { [string equal $pattern_ref $pattern_to_match] } {
			set match $name
			size_cell  $cell CORE65LPLVT/$name
			set final_leakage [get_attribute [get_cell $cell] leakage_power]
			return [expr $final_leakage - $initial_leakage]
		}
	}

}

####################################################################
#	procedure cell_swap_to_HVT { cell_to_swap }
#	# swap the cell
#	Return: difference of leakage
#
proc cell_swap_to_HVT { cell } {## size rimane uguale e cambia threshold voltage (LVT HVT)
	#set initial_leakage [get_attribute [get_cell $cell] leakage_power]
	set libcell [get_attribute [get_lib_cells -of_object $cell] base_name]
	set alternatives [get_alternative_lib_cells -libraries CORE65LPHVT $cell]
	foreach_in_collection el $alternatives {
		set name [get_attribute $el base_name]
		set pattern_to_match [string range $name end-3 end]
		set pattern_ref [string range $libcell end-3 end]
		if { [string equal $pattern_ref $pattern_to_match] } {
			set match $name
			size_cell  $cell CORE65LPHVT/$name
			#set final_leakage [get_attribute [get_cell $cell] leakage_power]
			#return [expr $final_leakage - $initial_leakage]
		}
	}
}

####################################################################
#	procedure get_leakage_power { }
#
#	Return: actual leakage power ()
#
proc get_leakage_power { } {
	report_power -significant_digits 9 > tmp
	set fp [open "tmp" r]
	set file_data [read $fp]
	close $fp
	set leakage 0
	set matchresult 0

	regexp {[ ]+[C][e][l][l][ ][L][e][a][k][a][g][e][ ][P][o][w][e][r][ =]+([0-9\-e\.]+)}  $file_data matchresult leakage
	file delete -force  tmp

	return $leakage
}

####################################################################
#	procedure cell_swap_to_bigger_size { cell_to_swap }
#   # swap to bigger size
#	Return: the difference of leakage
#
proc cell_swap_to_bigger_size { cell } {## size rimane threshold voltage (se era LVT rimane LVT) e cambia la size
	set libcell [get_attribute [get_lib_cells -of_object $cell] full_name]
	set max [get_lib_cells -of_object $cell]
	set library [string range $libcell 0 10]
	set alternatives [get_alternative_lib_cells -libraries $library $cell]
	foreach_in_collection el $alternatives {
		if { [get_attribute $el area]>[get_attribute $max area] } {
			set max $el
		}
	}
	size_cell $cell $library/[get_attribute $max base_name]
}

####################################################################
#	procedure cell_swap_to_smaller_size { cell_to_swap }
#   # swap to bigger size
#	Return: the difference of leakage
#
proc cell_swap_to_smaller_size { cell } {## size rimane threshold voltage (se era LVT rimane LVT) e cambia la size
	set libcell [get_attribute [get_lib_cells -of_object $cell] full_name]
	set min [get_lib_cells -of_object $cell]
	set library [string range $libcell 0 10]
	set alternatives [get_alternative_lib_cells -libraries $library $cell]
	foreach_in_collection el $alternatives {
		if { [get_attribute $el area]<[get_attribute $min area] } {
			set min $el
		}
	}
	size_cell $cell $library/[get_attribute $min base_name]
}

####################################################################
#
# return the actual reduction
#
#
proc get_reduction { pre_leakage  } {
	set post_leakage [ get_leakage_power ]
	set reduction [expr -($post_leakage - $pre_leakage) / $pre_leakage ]
	return $reduction
}

####################################################################
#
#	Optimization Algorithm
#
#
proc dualVth { leakageReduction } {
	set to_hvt 0
	set to_biggest 0
	set to_smaller 0

	set swapping_num 5; #number of cells swapped in one cycle
	set pre_leakage [ get_leakage_power ]
	set actualLeakage [ get_leakage_power ]
	set requestedLeakage [ expr $actualLeakage * (1 - $leakageReduction )  ]

	set out_pin_collection [get_pins -filter {direction==out}] ;#tutti i pin out non ordinati
	set cells_sorted [ get_cell -of_object [sort_collection $out_pin_collection {max_slack}] ]; #first min slack
	set i [ sizeof_collection $cells_sorted ]
	set i [ expr $i.0 * 0.05 ]

	foreach_in_collection cell $cells_sorted {
		set to_biggest [ expr $to_biggest + 1]
		cell_swap_to_bigger_size $cell
		set i [ expr  $i -1 ]
		if { $i <= 0 } {
			break;
		}
	}

	#low voltage threshold to high voltage threshold optimization
	set out_pin_collection [get_pins -filter {direction==out}] ;#tutti i pin out non ordinati
	set cells_sorted [ get_cell -of_object [sort_collection -descending $out_pin_collection {max_slack}] ]; #first max slack
	set i 30
	foreach_in_collection cell $cells_sorted {
		set to_hvt [ expr $to_hvt + 1]
		cell_swap_to_HVT $cell ; # PAX procedure

		set i [ expr $i -1 ]
		if { $i <= 0} {
			set i 30
			set actualLekeage [ get_leakage_power ]
			set reduction [ get_reduction $pre_leakage  ]
			if { $reduction > $leakageReduction } {
 			    break;
			}
		}
	}

	##last possibile solution if it is necessary (swap to smaller cell)
	set out_pin_collection [get_pins -filter {direction==out}] ;#tutti i pin out non ordinati
	set cells_sorted [ get_cell -of_object [sort_collection -descending $out_pin_collection {max_slack}] ]; #first max slack
	set actualLekeage [ get_leakage_power ]
	if { $actualLeakage > $requestedLeakage } {
		set to_smaller [ expr $to_smaller + 1 ]
		foreach_in_collection cell $cells_sorted {
			cell_swap_to_smaller_size $cell
			set reduction [ get_reduction $pre_leakage ]
			if { $reduction > $leakageReduction } {
				break;
			}
		}
	}

	set tot_number_of_cell [ sizeof_collection $cells_sorted ]

	puts "Total number of cell = $tot_number_of_cell"
	set frac_to_smaller [ expr  $to_smaller.0 / $tot_number_of_cell.0 * 100]
	set frac_to_biggest [ expr  $to_biggest.0 / $tot_number_of_cell.0 * 100 ]
	set frac_to_hvt [ expr  $to_hvt.0 / $tot_number_of_cell.0 * 100 ]

	puts "Total number of cell swapped to biggest size = $to_biggest: $frac_to_biggest %"
	puts "Total number of cell swapped to HVT          = $to_hvt    : $frac_to_hvt %"
	puts "Total number of cell swapped to smaller size = $to_smaller: $frac_to_smaller %"
}
