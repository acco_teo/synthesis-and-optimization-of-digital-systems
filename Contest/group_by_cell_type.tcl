set pre_leakage 0.0
global cell_grouped

foreach_in_collection point_cell [get_cells] {
	# calculating pre_optimisation leakage
	set pre_leakage [expr $pre_leakage + [get_attribute $point_cell leakage_power]]

	# getting the associative index in order to append in the right list 
	set cell_grouped(ID) [get_attribute $point_cell ref_name]
	
	puts $cell_grouped(ID)
	
	# appending the base_name (UXXXX) in the list 
	lappend cell_grouped($cell_grouped(ID)) [get_attribute $point_cell base_name]
}

parray cell_grouped

set list_cell_grouped [array get cell_grouped]
