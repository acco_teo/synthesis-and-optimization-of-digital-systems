# Low-Power Contest 18/19
### Synthesis and Optimization of Digital Systems
---
## Description
Write a command for PrimeTime that implements a post-synthesis power minimization procedure.<br
The new command, written in TCL, runs a leakage-constrained dual-Vth cell assignment with gate re-sizing. The only input argument is the leakage savings to be reached after the dual-Vth assignment process.<br>
It is measured as follows:
```math
saving = \frac{start\_power - end\_power}{start\_power}
```
where start_power is the leakage power consumption of the original circuit, while end_power is the leakage power consumption of the circuit after the optimization. The allowed values for the input argument savings may
range from 0 (no leakage minimization) to 1 (maximum leakage savings).
Note: only feasible constraints will be used for testing, namely, do not need to implement any feasibility check. <br><br>
**SYNOPSIS**
```tcl
dualVth –leakage $savings$
```
**EXAMPLE**
```tcl
dualVth –leakage 0.5 #50% of leakage savings w.r.t. the loaded design
```
Evaluation
The best algorithm is the one that matches the leakage savings constraint while reaching the smallest slack and
the minimal dynamic power penalty using the lowest amount of CPU time. The evaluation metrics are as follows:
1. compliance to the input constraint savings;
2. slack penalty due to leakage minimization (lower is better):
```math
slack\_penality =
  \begin{cases}
    0               & \quad \text{if } final\_slack \geq \text{ 0}\\
    -final\_slack   & \quad \text{if } final\_slack < \text{ 0}
  \end{cases}
```
3. dynamic power penalty (lower is better):
```math
dp\_penality = \frac{end\_power - start\_power}{start\_power}+1
```
4. execution time, that is the difference between *start-time* and *end-time* tcl clock command (lower is better).

Note: the algorithm must be general also in terms of timing constraint, that means the original circuit can be
synthesized with any clock period. It is possible to assume all the paths of the original circuit have slack >= 0.

## Basic Rules for the Competition
1. Combinational circuits used as benchmarks: {c1908.v, c5315.v} (*Note: the algorithm must be general and will be tested on other benchmarks, too*).
2. The command will be executed under PrimeTime, just after the script pt_analysis.tcl
3. The benchmark is first synthesized under a fixed timing constraint (e.g. clockPeriod= 3.0 ns) using
the synthesis.tcl with a single-VT target library, the CORE65\_LP\_LVT.
4. All the groups are invited (mandatory) to use the template available on the webpage of the course. Other
additional procedures can be used only if invoked within the dualVth procedure.

**Authors**
- Accornero Matteo (@acco\_teo)
- Costa Matteo (@costa\_teo)
- D'Angelo Paride (@paridang)

All rights reserved ©
