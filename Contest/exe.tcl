## put this in WORK_SYNTHESIS folder

proc get_slack {  } {

	report_timing -significant_digits 9 > tmp
	set fp [open "tmp" r]
	set file_data [read $fp]
	close $fp
	set leakage 0
	set matchresult 0

	regexp {[ ]+[s][l][a][c][k][ ]+[(][a-zA-Z]+[)][ ]+([0-9\-e\.]+)}  $file_data matchresult slack
	file delete -force  tmp

	return $slack

}

source ./scripts/pt_analysis.tcl
source ../contest.tcl

set pre_leakage [ get_leakage_power ]
set pre_slack [ get_slack ]

set start_time [clock seconds]

dualVth 0.07

set end_time [clock seconds]
set total_time [expr $end_time - $start_time]
puts "\nTotal time is $total_time"

set post_leakage [ get_leakage_power ]
set reduction [expr ($post_leakage - $pre_leakage) / $pre_leakage * 100 ]
puts "\nleakage reduction is: $reduction %"

set post_slack [ get_slack ]

puts "\npre_slack = $pre_slack\npost_slack = $post_slack"
