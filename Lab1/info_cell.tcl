set cell_list_name ""
set cell_list_ref ""
set cell_list_leakage ""
set cell_list_dynamic ""
set cell_list_area ""

foreach_in_collection point_cell [get_cells] {
           set cell_name [get_attribute $point_cell full_name]
           set cell_type [get_attribute $point_cell is_combinational]
           if { $cell_type == true } {
                lappend cell_list_name [get_attribute $point_cell base_name]
                lappend cell_list_ref [get_attribute $point_cell ref_name]
                lappend cell_list_leakage [get_attribute $point_cell leakage_power]
                lappend cell_list_dynamic [get_attribute $point_cell dynamic_power]
                lappend cell_list_area [get_attribute $point_cell area]
                puts "$cell_name is combinational"
           } else {
                puts "$cell_name is not combinational"
           }
}

puts "base_name: $cell_list_name"
puts "ref_name: $cell_list_ref"
puts "leakage_power: $cell_list_leakage"
puts "dynamic_power: $cell_list_dynamic"

set chip_leakage 0.0
foreach nxt $cell_list_leakage {set chip_leakage [expr $chip_leakage + $nxt]}
puts "chip_leakage: $chip_leakage"

set chip_dynamic 0.0
foreach nxt $cell_list_dynamic {set chip_dynamic [expr $chip_dynamic + $nxt]}
puts "chip_dynamic: $chip_dynamic"

set chip_area 0.0
foreach nxt $cell_list_area {set chip_area [expr $chip_area + $nxt]}
puts "chip_area: $chip_area"
