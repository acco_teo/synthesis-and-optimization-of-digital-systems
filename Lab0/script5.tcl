set par "Write a TCL script to: create a new list variable containing the numbers from 0 to 
10 (using the lappend command) displays the content of the list, one item per line 
(using the foreach command) calculate the script of the 5th and 10th element in the list and
append the script at the script of the list itself displays on the screen the last item of the
list"

set tmp [split $par " "]
puts [llength $tmp]

for {set i 0} {$i < [llength $tmp]} {incr i} {
	if {[lindex $tmp $i] eq "script"} {
		lset tmp $i "program"
	}
}

puts $tmp
