set size 32
set result {}
for {set y [expr {$size - 1}]} {$y >= 0} {incr y -1} {
    set line [lrepeat $size " "]
    for {set x $y} {$x < $size} {set x [expr {($x + 1) | $y}]} {
        lset line $x *
    }
    lappend result [string range [join $line " "] $y end]
}
puts [join $result \n]