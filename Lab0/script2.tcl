set a 1
set b 8
set c 4

set delta [expr ($b*$b - 4*$a*$c)]

if {$delta > 0} {
	set x1 [expr (-$b + sqrt($delta))/(2*$a)]
	set x2 [expr (-$b - sqrt($delta))/(2*$a)]
	puts $x1
	puts $x2
} else {
	puts "The equation has no real solutions"
}
