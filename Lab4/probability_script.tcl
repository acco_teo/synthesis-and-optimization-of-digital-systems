set fid [open "aes_cipher_top_swa.txt" "r"]
set line ""
set matchresult 0
set name  0
set time1 0
set time0 0
set timex 0
set tc    0
set p 	  0
set tsim 0

while {[gets $fid line] != -1} {
	regexp {/[a-z|A-Z|0-9]+/[a-z|A-Z|0-9]+/([a-z|A-Z|0-9|(|)|_]+)[ ]+([0-9]+)[ ]+[0-9]+[ ]+([0-9]+)[ ]+([0-9]+)[ ]+([0-9]+)} $line matchresult name tc time1 time0 timex

	if { $matchresult ne 0 } {
		set matchresult 0

		set tsim [ expr $time0 + $time1 + $timex ]
		set p [ expr $time1.0 / $tsim  ]
		puts "name: $name\n\ttc: $tc\n\tP: $p\n\tTsim: $tsim\n\n"

		set_switching_activity "$name" -static_probability "$p" -toggle_rate "$tc" -period "$tsim"

	}

}
